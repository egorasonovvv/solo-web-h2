package com.example.soloweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoloWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoloWebApplication.class, args);
    }

}
